package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.GameController;
import model.SudokuGrid;

/**
 * Graphic container element for the 
 * @author jmalvarez
 *
 */
public class GameFrame extends JFrame implements GameView {

	private static final long serialVersionUID = -3610405468814426911L;
	private GridPanel panel;

	public GameFrame(String title, SudokuGrid model) {
		super(title);
		
		panel = new GridPanel(model);
		setContentPane(panel);
		pack();
		// setIgnoreRepaint(true);
		setFocusable(true);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * This method shows a graphic dialog with the given message.
	 * @param m. Message string to be shown.
	 */
	@Override
	public void showMessage(String m) {
		JOptionPane.showMessageDialog(this, m);
	}

	/**
	 * This method attaches the controller to the corresponding panel.
	 * @param gc. Controller to be attached.
	 */
	@Override
	public void attachController(GameController gc) {
		panel.attachController(gc);
		
	}

}
