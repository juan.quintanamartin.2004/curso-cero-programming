package view;

import java.util.Scanner;

import controller.GameController;
import model.CellState;
import model.SudokuGrid;
import utils.PlayerAction;
import utils.PlayerChoice;

public class ConsoleView implements GameView{

	// ANSI escape codes to change color on console
	// For these codes to work properly in the Eclipse console, 
	// the plugin "ANSI Codes in Console" must be installed 
	// through the Eclipse Marketplace.
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_BLACK = "\u001B[30m";
	
	SudokuGrid sudoku;
	GameController control;
	
	Scanner keyboardScanner;
	
	public ConsoleView(SudokuGrid sg) {
		sudoku = sg;
		control = null;
		keyboardScanner = new Scanner(System.in);
	}

	/**
	 * This method attaches the controller to the corresponding panel.
	 * @param gc. Controller to be attached.
	 */
	@Override
	public void attachController(GameController gc) {
		control = gc;
	}

	/**
	 * This method shows a string message on console.
	 * @param m. String message to be shown.
	 */
	@Override
	public void showMessage(String m) {
		System.out.println(m);
		
	}

	public void play() {
		while(true) {
			System.out.println(sudoku.toString());
			PlayerChoice pc = getPlayerChoice();
			switch(pc.getAction()) {
			case SETVALUE:
				control.setValueAt(pc.getValue(), pc.getRow(), pc.getColumn());
				break;
			case EMPTYCELL:
				control.emptyCell(pc.getRow(), pc.getColumn());
				break;
			default:
				break;
			}
		}
	}
	
	public PlayerChoice getPlayerChoice() {
		char choice;
		int value = 0;
		int row = 0;
		int column = 0;
		do {
			System.out.print("Choose your action: (S)et a value/ (E)mpty a cell. ");
			choice = Character.toUpperCase(keyboardScanner.next().charAt(0));
			String str = keyboardScanner.nextLine();
			System.out.println("");
		}while (choice != 'S' && choice != 'E');

		do {
			System.out.print("Choose row (between 1 and 9): ");
			if (keyboardScanner.hasNextInt()){
				row = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (row < 1 || 9 < row);

		do {
			System.out.print("Choose column (between 1 and 9): ");
			if (keyboardScanner.hasNextInt()){
				column = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (column < 1 || 9 < column);

		PlayerChoice pc;
		if (choice == 'S') {
			do {
				System.out.print("Choose a new value for the cell (between 1 and 9): ");
				if (keyboardScanner.hasNextInt()){
					value = keyboardScanner.nextInt();
				} else {
					keyboardScanner.nextLine();
				}
				System.out.println("");
			}while (value < 1 || 9 < value);
			pc = new PlayerChoice(PlayerAction.SETVALUE, value, row - 1, column - 1);
		} else {
			pc = new PlayerChoice(PlayerAction.EMPTYCELL, 0, row - 1, column - 1);
		}
		
		return pc;
	}

	
}
