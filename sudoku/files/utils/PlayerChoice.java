package utils;

public class PlayerChoice {
	private PlayerAction action;
	private int value;
	private int row;
	private int column;

	public PlayerChoice(PlayerAction act, int v, int r, int c) {
		action = act;
		value = v;
		row = r;
		column = c;
	}

	public PlayerAction getAction() {
		return action;
	}

	public int getValue() {
		return value;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
	
	

}
