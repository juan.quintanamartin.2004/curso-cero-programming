package main;

import model.SudokuGrid;

public class MainRevisitedModelPart2 {

	public static void printFullHouse(SudokuGrid sd, int x, int y) {
		if (sd.isFullHouse(x,y)) {
			System.out.println("Cell " + x + ", " + y + " is a Full House cell\n");
		}else {
			System.out.println("Cell " + x + ", " + y + " is not a Full House cell\n");
		}
	}
	
	public static void printNakedSingle(SudokuGrid sd, int x, int y) {
		int nakedSingle = sd.nakedSingle(x,y);
		if (nakedSingle != 0) {
			System.out.println("Cell " + x + ", " + y + " has a naked single: " + nakedSingle + "\n");
		}else {
			System.out.println("Cell " + x + ", " + y + " has not a naked single\n");
		}
	}
	
	public static void main(String[] args) {
		int gridContents [][] = {
				{0,6,0,1,0,4,0,5,0},
				{0,0,8,3,0,5,6,0,0},
				{2,0,0,0,0,0,0,0,1},
				{8,0,0,4,0,7,0,0,6},
				{0,0,6,0,0,0,3,0,0},
				{7,0,0,9,0,1,0,0,4},
				{5,0,0,0,0,0,0,0,2},
				{0,0,7,2,0,6,9,0,0},
				{0,4,0,5,0,8,0,7,0}
				};
		
		SudokuGrid sudoGrid = SudokuGrid.createNewGame(gridContents);
		
		System.out.println(sudoGrid.toString());
		
		sudoGrid.fillSudokuGrid(true);
		
		sudoGrid.emptyCell(2, 3);
		
		System.out.println(sudoGrid.toString());
		
		printNakedSingle(sudoGrid,2,3);
		

		sudoGrid = SudokuGrid.createNewGame();
		
		System.out.println(sudoGrid.toString());
		
		printNakedSingle(sudoGrid,1,7);
		

		sudoGrid.fillSudokuGrid(true);
		
		sudoGrid.emptyCell(2, 3);
		
		System.out.println(sudoGrid.toString());
			
		printFullHouse(sudoGrid,2,3);
		
		sudoGrid.emptyCell(5, 7);
		
		System.out.println(sudoGrid.toString());
		
		printFullHouse(sudoGrid,5,7);
		
	}

}
