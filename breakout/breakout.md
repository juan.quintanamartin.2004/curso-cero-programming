## The Breakout Game

[!["Breakout game"](images/breakout.png "Breakout game")](files/breakout.jar)
<!--
<p align="center">
  [<img src="images/breakout.png"
      alt="Breakout game"
      width="400"/>](files/breakout.jar)
</p>
-->

Breakout is an arcade game originally developed by Atari Inc. in 1976.
In the game, there is a paddle, a ball and bricks.
Bricks are positioned at the top of the window, organized in rows and columns.
A ball moves freely around the playing area, bouncing at the top, left and right sides.
The ball also bounces when it finds one of the bricks, but the brick gets broken.
However, the playing area is open at the bottom.
To avoid the ball to leave the playing area, what would terminate the game, there is a paddle operated by the user.
If the ball reaches the paddle, it bounces back into the playing area.
The player moves the paddle only to the left and to the right using the cursor keys.
The goal of the game is to destroy all bricks in the top of the window.

### General Description of the Implementation

One of the keys of object-oriented programming and design is that _objects_ in the real world (_the problem domain_) are represented by _objects_ in the implementation of the system (the _solution domain_).
Hence, we will have in our implementation of the game objects representing paddles, balls and bricks.
And not only visible objects, other objects like panels, frames, timers and schedulers will help us to model the system at hand.

Of course, if we are going to have 30 bricks in our game, we do not want to repeat their implementation 30 times.
We want to implement the behaviour of brick objects once, and then be able to create as many instances as we wish.
That is what we get with _types_ and _classes_.
Many types and classes are predefined, but we can define our own to solve specific problems.
For example, a class `Brick` will implement the state and behaviour of bricks, and we will be able to create as many instances of this class as we want.
We will have brick _objects_ created as instances of the `Brick` class.

!["`Brick`, `Ball` and `Paddle` classes"](images/brick-ball-paddle-small.png "Brick, Ball and Paddle classes")
<!--
<p align="center">
<img src="images/brick-ball-paddle.png"
     alt="`Brick`, `Ball` and `Paddle` classes"
     width="300"/>
</p>
-->

Moreover, there are classes that may share some features.
For instance, paddles, balls and bricks have a graphical representation.
Instead of repeating the corresponding code for each of them, we may _abstract_ such a behaviour into a `Sprite` class and then define `Paddle`, `Ball` and `Brick` as subclasses of `Sprite`.
In this way, we can define the common behaviour in the `Sprite` class, and then _inherit_ it in each of its subclasses.
This inherited features may be later changed or completed, to define specializations.
In our case, an instance of class `Ball` will be a ball, but all balls, bricks and paddles will also be sprites.

!["Sprite classes"](images/sprite-brick-ball-paddle-small.png "Sprite classes")
<!--
<p align="center">
  <img src="images/sprite-brick-ball-paddle.png"
       alt="Sprite classes"
       class="center"
       width="300"/>
</p>
-->

Before getting into further details on classes and how classes are defined, let us look at how objects communicate.
In object-oriented programming, we say that objects communicate via _messages_.
Suppose that you want to turn on the light of your room.
You have to press the switch to turn on the light.
From an object-oriented perspective, we may see this process as a two-step process.
First, "a hand sends a press message to the switch".
Upon the reception of this message, the switch will handle it by sending another message: "the switch sends a turn-on message to the light bulb".
Note that we have also identified the objects involved in the process: hand, switch and light bulb.
We have also model how the interact via messages to solve a given problem.
Something similar will happen in our game.

!["Message passing"](images/message-passing-small.png "Message passing")
<!--
<p align="center">
  <img src="images/message-passing.png"
     alt="Message passing"
     width="300"/>
</p>
-->

Also, before describing the communications in our game, we need to introduce some structure.
Specifically, we will follow the model-view-control (MVC) design pattern, a classical pattern for the design of applications with graphical user interface (GUI).
With it, we structure our system in three components: the _model_ represents the data structures and logic of the application, the _view_ represents the graphical visualization of the model, and the _controller_ gets the user input and act on the model and view.
In this case, we will have that when we press or release a cursor key (an event has occurred), a message is sent to the corresponding controller, which acts on the model, to change the direction of the movement of the paddle.
In addition, a scheduler object (a timer task) will take control periodically, moving the paddle and the ball, and checking for collisions (the ball may reach a wall, a brick or the paddle), and updating the score.

!["MVC design pattern"](images/mvc-small.png "MVC design pattern")
<!--
<p align="center">
<img src="images/mvc.png"
     alt="MVC design pattern"
     width="300"/>
</p>
-->

Let us introduce some final details before going for the implementation.
Each object in the game is visualized with a corresponding image.
These objects are then placed in panels and frames for their visualization.

The movement of the paddle and the ball is implemented as follows.
A paddle has a direction `dx`, which can be either `-1`, `0`, or `1`, depending on whether its moving to the left, stopped, or moving to the right.
A _key listener_ object is in charge of acting upon the pressing or releasing of keys.
If a left (resp. right) cursor key is pressed, the direction of the paddle is set to `-1` (resp. `1`).
When a left or right cursor key is released, the direction of the paddle is set to `0`.

The movement of the ball is implemented in a similar way, although in this case direction changes happen when the ball reaches an obstacle.
The direction of the ball is represented by a vector with coordinates `(dx,dy)`, where `dx` and `dy` take values `1` or `-1`.
That is, the ball follows directions `(1,1)`, `(-1,1)`, `(-1,-1)` or `(1,-1)`.
When the ball reaches an obstacle it bounds, changing its direction correspondingly.

The up-left corner of the screen is assumed to be the position `(0,0)`.
`dy=1` means downwards, and `dy=-1` means upwards

!["Directions"](images/directions-small.png "Directions")
<!--
<p align="center">
<img src="images/directions.png"
     alt="Directions"
     width="150"/>
</p>
-->

To implement the different activities going on in the game, the positions of the different sprite objects are updated periodically, in accordance to their current positions and directions.
A _timer task_ fires the execution of a periodic task, which moves the paddle and the ball, and checks whether the ball has reached a side of the playing area, a brick (which is then destroyed), or the paddle.

The actual implementation of the MVC pattern for our game will be as shown below. The `Breakout` class implements the model, and the `GameFrame` class implements the frame with the visualization of the game. The `PaddleController` is the key listener that attends the pressing and releasing of the keys, and `PeriodicTask` is the task in charge of implementing the periodic tasks of the game.

!["Breakout's MVC"](images/mvc-breakout-small.png "Breakout's MVC")
<!--
<p align="center">
<img src="images/mvc-breakout.png"
     alt="Breakout's MVC"
     width="500"/>
</p>
-->

> ##### Task 1
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Open Eclipse.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    If there is no active workspace, create one. This is only a folder where all the files of your projects are together.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Create a new Java project named `breakout`.
>    Accept the default options offered by Eclipse, but reject the proposal to create a file `module-info.java`.
> 4. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    In this newly created project, create a new package named `breakout`.
>    Accept the default options offered by Eclipse.

### Model: The `GameModel` Interface and the `Breakout` Class

The `GameModel` interface defines the set of methods that the model of the application, in this case the `Breakout` class, must implement.
A `Breakout` object has several sprites: a ball, a paddle and a set of bricks.
Let us implement these classes before implementing the `Breakout` class.

> ##### Task 2
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`GameModel.java`](files/GameModel.java) file in the package `breakout`.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Note the error marks on the side of the file.
>    We will ignore them for now, they will vanish as we create classes `Brick`, `Paddle`, and `Ball` below.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the definition of the interface `GameModel`, and observe the way in which comments are written to document the different elements in the interface.
>    Although there are other ways to write comments in Java, namely `//` for a line comment, or `/*` and `*/` for block comments, the use of `/**` and `*/` allows us to use javadoc comments that will be used in the IDE to provide on-line information for our classes.
>    The API documentation of Java itself is created using the comments introduced in this way, and we could generate similar HTML documentation for our developments.

#### The `Sprite` Class

A sprite is a visible object in a game. They have a position `(x,y)` in a bidimensional space.
They also have an associated image, which always has a rectangular shape.
Therefore, the image will have a width and a height, and we will use the rectangle shape of the image to detect collisions between objects by checking the intersections between their corresponding rectangles.
One of these rectangles is defined by the coordinates of its top-left corner and its width and height.

In the following example we can see a Panel with a `width=325` and a `height=400`. This means that the top left coordinate is the `(0,0)` and the right botton is the `(325,400)`. We can put sprites inside this range. For example if a brick has the size `(42,20)` if we put one in the position `(15,50)` and we want to put another one at its right. It should be at `(15+42,50)` as can be seen in the image.

!["Breakout Coordinates"](images/breakout-coordinates.png "Coordinates of the canvas")



In Java, objects are created using their _constructors_.
A class may have several constructors, each of which may take zero or more arguments of different types with which the state of the object being created is to be initialized.
Constructors are defined (and invoked) using the name of the class.
The `Sprite` class has one single constructor, `Sprite(int nx, int ny, String f)`, that takes as arguments the position to place the sprite (`(nx, ny)`), and the name of the file, `f`, containing the  image to visualize it.
The `Sprite` class is an abstract class that provides the following methods:
- `setX(int)` and `setY(int)` set the `x` and `y` components of the position of the sprite.
- `getX()`, `getY()`, `getWidth()`, `getHeight()`, and `getImage()` return the corresponding attributes of the sprite.
- `getRectangle()` returns a `Rectangle` object that embeds the sprite.
- A redefinition of the `toString()` method provides a representation of the object as a string.

> ##### Task 3
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`Sprite.java`](files/Sprite.java) file in the `breakout` package.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the class definition and complete the parts marked as `TO BE COMPLETED`.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place your mouse over the constructor of the `Rectangle` class to get information on the predefined class and the meaning of its arguments. Do the same over the `getWidth` message sent to the `image` object.
> 4. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Notice that by the way in which the comments are provided for the different classes, variables and methods in this implementation, we can get similar information when placing the mouse over any of these elements.

#### The `Brick` Class

Bricks are static elements, and everything they need is already there by just inheriting from the `Sprite` class.
Note that its constructor just invokes the constructor in the superclass to initialize its state.

> ##### Task 4
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`Brick.java`](files/Brick.java) file in the `breakout` package.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Check that some of the error marks in the `GameModel` interface are now gone.
>    Since it can now find the definition of the `Brick` class this mark is gone.
>    The same will happen as we had the rest of the necessary classes.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the class definition.

#### The `Paddle` Class

Paddles are sprites that move along the x axis.
They have a direction, `dx`, which can be either `-1` (to the left), `0` (stopped), or `1` (to the right).
Like the `Sprite` class, the `Paddle` class provides a constructor with three arguments, which initializes the position and image of new paddle objects as for any other sprite. Also, it initializes its direction with value 0 (paddles are initially stopped).
In addition it provides the following methods:
- `move()` updates the position of the paddle object taking into account its current position and its direction. E.g., if a paddle is located at position `x=25` and has direction `dx=1`, upon the reception of a `move()` message, its position would be updated to `x=26`. Of course, the y component of its position does not change. The method also checks whether such new position is valid. As we will see below, the `GamePanel` class provides constants with the left and right limits.
- The `setDirection(int)` method updates the `dx` variable with the value passed as argument.

> ##### Task 5
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the files [`Paddle.java`](files/Paddle.java) and [`GamePanel.java`](files/GamePanel.java) in the `breakout` package.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the definition of the class `Paddle` and complete the parts marked as `TO BE COMPLETED`.

#### The `Ball` Class

Balls are sprites that move along the x and y axis. They have a direction, `(dx, dy)`, which can be either `(1, 1)`, `(1, -1)`, `(-1, 1)` or `(-1, -1)`.

In addition to a constructor as the ones for the other sprites, the class provides the following methods:
- `move()` updates the position of the ball object, modifying the `x` and `y` coordinates of the objects correspondingly.
- The `setDirectionX(int)` and `setDirectionX(int)` methods update the `dx` and `dy` variables, respectively, with the values passed as arguments.
- The `getDirectionX()` and `getDirectionY()` methods return the corresponding component of the direction of the object.

> ##### Task 6
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`Ball.java`](files/Ball.java) file in the `breakout` package.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the class definition and complete the parts marked as `TO BE COMPLETED`.

#### The `Breakout` Class

Now that we have definitions of the classes for the different sprites, we can implement the `Breakout` class.
A `Breakout` object has a ball, a paddle and a set of bricks.
Initially, 30 bricks are created, in their corresponding positions, and included in a set.
When a brick is destroyed, the brick is removed from the set.
When the set is empty the game terminates.
A boolean variable `gameover` has value `true` if the game is over, or `false` otherwise.
The state of the variable is checked by the view to produce the right message when it ends.
Finally, instances of the `Breakout` class keep track of number of bricks destroyed, which represents the `score` of the game.

The class provides:
- A constructor with no arguments. When a `Breakout` game is created, we create a ball, a paddle and 30 bricks, organized in 5 rows and 6 columns, each in its corresponding position. The score is initialized to 0.
- Methods `getBall()`, `getPaddle()`, `getBricks()`, `getScore()`, and `gameOver()` return the values of the corresponding attributes.
- The method `gameOver(boolean)` updates the value of the `gameover` variable with the value passed as argument.
- The method `checkCollision()` checks whether the ball has reached a side of the playing panel or has collided with the paddle or a brick.
  If the ball reaches the top, left or right sides, its direction is changed.
  The direction of the ball also changes if it touches the paddle or a brick.
  To detect such collisions, we  check whether the rectangles of the ball and the other object intersect.
  If the ball's rectangle intersects with the rectangle of one of the standing bricks, the brick is removed from the set of standing bricks and the score is incremented.

> ##### Task 7
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Create a new package `breakout.images`, a place the files [`brickr.png`](files/brickr.png), [`bricky.png`](files/bricky.png),[`brickrg.png`](files/brickrg.png),[`brickrp.png`](files/brickrp.png),[`paddle.png`](files/paddle.png), and [`ball.png`](files/ball.png) into it.
> 2. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`Breakout.java`](files/Breakout.java) file in the `breakout` package.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Read the class definition and complete the parts marked as `TO BE COMPLETED`.
>    Pay special attention to the use of the predefined definition of sets (using interface `Set` and its implementation `HashSet`, adding elements using its `add` method, and using iterators to go through its elements) and the use of the rectangles associated to the sprites to check collisions as their intersections.


### View: The `GameView` Interface and the `GameFrame` Class

The `GameFrame` class implements the `GameView` interface. In addition to a `repaint()` method, which is key for the periodic repainting of the visualization of the game, the interface includes methods `setMessage(String)`, to set the message to print upon the finalization of the game (depending on whether the player wins or loses), and `updateScore(int)` to maintain the score shown up to date.

The structure of the `GameFrame` class is rather complex, as are typical classes defining GUIs. Java provides many basic components, and we will have an appearance or another depending on how we combine them. In our case, as we can see in the image below, we have that an instance of the `GameFrame` class, which inherits from the `JFrame` predefined class, has a contents pane provided by an instance of class `MainPanel`. `MainPanel` is a `JPanel` with a `BorderLayout` layout, with a panel in its north (with the label `"Score: "` and a `JTextField` with the current score) and another one in its center. The panel in its center is an instance of the `GamePanel` class, which contains the images of the sprites in the game.

!["Structure of the Breakout GUI"](images/gui-small.png "Structure of the Breakout GUI")
<!--
<p align="center">
  <img src="images/gui.png"
       alt="Structure of the Breakout GUI"
       width="500"/>
</p>
-->

Let us not worry on the construction of the GUI itself.
Just notice that when the `GameFrame` object gets `setMessage(String)` or `updateScore(int)` messages, it just re-sends them to its inner `MainPanel` panel.
The `MainPanel` object will update the shown score upon the reception of a `updateScore(int)` message, and will re-send `setMessage(String)` messages to its embedded `GamePanel` panel.
The `GamePanel` is indeed the main visual element in the game, since it is in charge of accessing the positions and images of each of the active elements in the game and visualizing them.
Classes `JFrame` and `JPanel` provide implementations of the `repaint()` method, which basically repaints all its components and sends a `paintComponent(Graphics)` message to itself.
The `paintComponent(Graphics)` method is therefore painting the panel with the desired aspect.

Let us then focus on the `GamePanel` class. In addition to its constructor, the class provides several constants and instance variables, and implementations of the methods `paintComponent(Graphics)` and `setMessage(String)`. The main constants in this class are the dimensions of the paying area: `WIDTH`, defined as `325`, and `HEIGHT`, as `400`. The other constants are defined to facilitate their use in the different parts of the code: bottom of the area for the control of the ball (`BOTTOM = HEIGTH - 10`), limits for the paddle (`PADDLE_LEFT_LIMIT = 2` and `PADDLE_RIGHT_LIMIT = WIDTH - 50`), and limits for the ball (`BALL_LEFT_LIMIT = 0` and `BALL_RIGHT_LIMIT = WIDTH - 20`). Instance variables `model` and `message`, respectively, keep a reference to the `Breakout` object and the message to be printed upon the finalization of the game.

The `setMessage(String)` method simply updates the `message` instance variable. The interesting method is `paintComponent(Graphics)`, which is in charge of the actual painting of the panel every time a repaint is requested. If the game is over (checked using the `gameOver()` method of the `GameModel` interface), a final message is printed. Otherwise, the ball, the paddle and the standing bricks are drawn in the panel in their respective positions over the graphical representation of the panel (a `Graphics` object received as argument).  

> ##### Task 8
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the files [`GameView.java`](files/GameView.java), [`GameFrame.java`](files/GameFrame.java), and [`MainPanel.java`](files/MainPanel.java) in the `breakout` package.
> 2. !["yellowpepper"](images/yellowpepper-15.png "Breakout")
>    Read the class definitions.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the [`MainView.java`](files/MainView.java) file in the `default` package (in the `src` folder of the project). Read the class definition.
>    As the above `MainModel` class, it implements an application that creates an instance of the `Breakout` class and sends `move()` messages to the ball until the game is over.
>    However, in this case, an instance of the `GameFrame` class is created to visualize the evolution of the game.
> 4. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Notice the use of the `repaint()` messages in the `MainModel` class to update the state of the visual representation of the game (instead of the `println` messages used above) in each iteration of the `while` loop.
> 5. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Notice also the use of the `Thread.sleep(10);` command to visualize the execution.
>    Comment out the line (placing `//` in its beginning) and execute the class.
>    If you did it right, you directly saw the final message, you miss the rest.
>    It is ok, everything went so quickly that we were not able to see anything.  
>    The `sleep` method stops the execution for the specified number of milliseconds.
>    Try with different values and see what happens.
> 6. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Modify the code of the `MainView` class so that the ball rebound when it gets to the botton. (for this, when the ball gets to the botton, change the direction of the ball instead of activate gameover --> : `model.getBall().setDirectionY(-1);`)


### Control: The `PaddleController` Class

The `PaddleController` class controls the pressing and releasing of keys.
Specifically, we will deal with the pressing and releasing of the left and right cursor keys, ignoring any other actions on other keys.
To do that, the `PaddleController` class must implement the `KeyListener` interface.
However, since we only want to handle a few key actions, instead of implementing the interface directly, the `PaddleController` class inherits from the `KeyAdapter` class, which provides default implementations to all the necessary methods.
Therefore, the `PaddleController` class only needs to implement those methods we need.
Specifically, in addition to a contructor, with which the variable `model` to store a reference to the model object, it provides redefinitions of the methods `keyPressed(KeyEvent)` and `keyReleased(KeyEvent)`.

> ##### Task 9
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the file [`PaddleController.java`](files/PaddleController.java) in the `breakout` package.
> 2. !["yellowpepper"](images/yellowpepper-15.png "Breakout")
>    Read the class definitions.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Observe the definitions of the `keyPressed(KeyEvent)` and `keyReleased(KeyEvent)` methods, and see how the `KeyEvent` object that is passed as argument of these methods carries information on the key that is pressed or released.
>    Observe also how we can identify the key being pressed by accessing to the key's code (with the `getKeyCode()` method).
>    The `KeyEvent` class has definitions of contants for each of the keys, specifically, `KeyEvent.VK_LEFT` and `KeyEvent.VK_RIGHT` represent the codes of the left and right cursor keys (non-numpad arrow keys), repectively.
>    Notice how the direction of the paddle is changed depending on the key and action.
> 4. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Complete the parts marked as `TO BE COMPLETED`.
> 5. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    What happen if several keys are pressed or released "simultaneously"?
> 6. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    What about changing the values set as arguments of the `setDirection` messages? What about using, e.g., `-2` or `-3` instead of `-1` when the left cursor is pressed?

### Control: The `PeriodicTask` Class

The control of the keys carried out by the `PaddleController` class only changes the direction of the paddle, but it does not move it. Indeed, nothing moves with what we have implemented until now. The sensation of action that we observe in games typically happens thanks to some periodic activity: repeat some action every certain amount of time.

In our case, we use a periodic task that fires some action.
We use the `TimerTask` class, and associate a scheduler with a fixed rate. To do this, our class `PeriodicTask` uses  the class `Timer` (see the `timer` instance variable) that starts an object of class `TimerTask` periodically, by invoking the method `scheduleAtFixedRate()`. The last parameter indicates the period at which the object is started. That value is set to 10 milliseconds. Every time this object is started, its method `run()` is executed. That method calls, in turn, to the method `executeStep()` of the class `PeriodicTask`, and a  will wake up the `PeriodicTask` object.

The `executeStep()` method moves the ball, the paddle, and checks for the end of the game (whether the number of remaining bricks is zero or not, and whether the ball has reached the bottom of the playing area).
If the game has not finished, the controller checks for collions, updates the score, and finally repaints the state of the game.
Note that if the end of the game is detected, the periodic scheduler action is cancelled (`timer.cancel()`), ending the game.

> ##### Task 10
> 1. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Place the file [`PeriodicTask.java`](files/PeriodicTask.java) in the `breakout` package and the file [`Main.java`](files/Main.java) in the `default` package.
> 2. !["yellowpepper"](images/yellowpepper-15.png "Breakout")
>    Read the class definitions.
> 3. !["greenpepper"](images/greenpepper-15.png "Breakout")
>    Make the necessary changes so that when the player loses, instead of the message "Game over!", he/she gets a "You lose!" message.
> 4. !["yellowpepper"](images/yellowpepper-15.png "Breakout")
>    How can we make the game go slower or quicker?
>    (Hint: Try changing the period of the timer used in the `PeriodicTask` class.
>    Alternatively, try changing the `move()` methods of the `Paddle` and `Ball` classes.)

>> 4.1 The change the ball or paddle speed, we need to multiply the value asigned for x and y. For example, to duplicate the speed of the ball instead of `x+=dx;` we can use `x+=dx*2;`

>> 4.2 !["redpepper"](images/redpepper-15.png "Breakout") Instead of putting the 2, we can define an attribute `speed` in the clases `ball` and `paddle` that could be initialized in the constructor for example changing `public Ball(int nx, int ny, String file)` for public `Ball(int nx, int ny, int nspeed, String file)`  then the creation of the ball in `breakout.java` would call the ball `ball = new Ball(230, 355, gameSpeed, ballImg);`  where gameSpeed is an int variable with the speed of the ball.

> 5. !["redpepper"](images/redpepper-15.png "Breakout")
>    When all bricks are destroyed the game finishes.
>    Instead, we could make the game start over, re-creating the bricks and make the game go on.
>> 5.1. Create a method `void nextLevel()` in game model and implement it in `Breakout` like this.

~~~	
public void nextLevel() {
		this.createBricks();
		stage++;
	}
~~~

>> 5.2. In the `PeriodicTask->Run` change after the check of bricks==0. Instead of gameOver-->`model.nextLevel()`.

>> 5.3 If we also want that all levels to be different in BreakOut-->createBricks change the line: 
>>`bricks.add(new Brick(j * 42 + 15, i * 22 + 50, brickImg[i % 4]));`
>>by:
`if (r.nextBoolean()) {
					bricks.add(new Brick(j * 42 + 15, i * 22 + 50, brickImg[i % 4]));
				}`

>>In this code, r (that is a Random generator, will return true or false randomly, and the code inside the if statement will be executed only if it is true. The result is that the bricks will be only generated sometimes).

>> 5.4 Note in the constructor of Breakout that r is init as: `r=new Random(1);` this number is the random seed, and if it is a constant the sequence of random numbers will always be the same. So you can change to see different variation of levels.

> 6. !["redpepper"](images/redpepper-15.png "Breakout")
>    We can make that, if the bricks are restored, the game goes a bit faster as well, so that we make sure the game is eventually so fast that it finishes.
> To achieve this we could create a public method in `ball` called `incSpeed()` that would increment the speed of the ball:

~~~
public void incSpeed() {
    speed++;
}
~~~

> That will be call from `Breakout->nextLevel()`. Note that if we increase the speed in every level the ball will go too fast in few levels. To solve that we could increase the speed for example every 5 levels. We can do it with a conditional statement:

~~~
if (stage%5==0) {
	ball.incSpeed();
}
~~~

> Look at it to provide the appropriate method implementations in classes `GameFrame` and `MainPanel`.)

> 7. !["yellowpepper"](images/yellowpepper-15.png "Breakout")
>    Make the necessary changes to the classes implementing the game so that we can move the paddle up and down as well?
>    (Hint: The key codes for the up and down keys are `KeyEvent.VK_UP` and `KeyEvent.VK_DOWN`.
>    You must modify the `keyPressed` and `keyReleased` methods in the `PaddleController` class to control these additional keys.
>    Moreover, you need to change the direction in which the paddle can move.
>    As for the ball, the paddle now needs an `x` direction and a `y` direction, which should have some limit to make sure it does not go over the upper and lower limits.)

> 8. !["redpepper"](images/redpepper-15.png "Breakout")!["redpepper"](images/redpepper-15.png "Breakout")!["redpepper"](images/redpepper-15.png "Breakout")
>    Make the necessary changes so that the ball has richer movements.
>    Instead of directions `(1,1)`, `(-1,1)`, `(-1,-1)` or `(1,-1)`, we can consider a vector `(x,y)` with `x` and `y` any integer value.
>    The changes of direction will then need to take this into account.
> Hint: In `Breakout->checkCollision()` the action if the ball intersect with the paddle should be changed instead of: `ball.setDirectionY(-1);` the value of -1 could be changed by a random number between -2 and -1. To do that the sentence would be:  `ball.setDirectionY(r.nextInt(-2, 0));`. Try different values and change also the direction of X.
>    We could then have more interesting bouncings, which may depend on the speed of the paddle, the part of the paddle that collision or even playing with rotation effects by using an angular speed: physics! 
>    Another possibility could be to consider the direction of the ball as given by a vector with magnitude 1 and some angle.

### Final Class Diagram
All those classes are related as shown in this images. The first one show only the clases:
!["Structure of the Breakout GUI"](images/breakoutDiagram1.png "Diagram Class simplified")

The second one shows all the attributes and methods inside the classes.
!["Structure of the Breakout GUI"](images/breakoutDiagramBig1.png "Structure of the Breakout GUI")
