package breakout;

/**
 * Balls are sprites that move along the x and y axes. They have a direction,
 * (dx, dy), which can be either (1, 1), (1, -1), (-1, 1) or (-1, -1).
 */
public class Ball extends Sprite {

	/**
	 * The direction of the ball is given by a vector of coordinates (dx, dy), where
	 * dx and dy will take values 1 or -1, i.e., we only have four possible
	 * directions: (1, 1), (1, -1), (-1, 1) and (-1, -1)
	 **/
	private int dx, dy;

	/**
	 * A ball is created in the position passed as argument, and using the image in
	 * the file to visualize it. A ball is created with direction (1,-1), that is
	 * upwards and to the right.
	 *
	 * @param nx   is the initial value for the x coordinate
	 * @param ny   is the initial value for the y coordinate
	 * @param file is the file that contains the image to represent the
	 *             visualization of the sprite
	 */
	public Ball(int nx, int ny, String file) {
		super(nx, ny, file);
		dx = 1;
		dy = -1;
	}

	/** Moving a ball just increments its coordinates in the current direction **/
	public void move() {
		x += dx;
		y += dy;
	}

	/**
	 * Sets the x component of the direction of the object.
	 *
	 * @param ndx new x component of the direction.
	 */
	public void setDirectionX(int ndx) {
		// TO BE COMPLETED
	}

	/**
	 * Sets the y component of the direction of the object.
	 *
	 * @param ndy new y component of the direction.
	 */
	public void setDirectionY(int ndy) {
		// TO BE COMPLETED
	}

	/**
	 * Returns the x component of the direction of the object.
	 *
	 * @return x component of the direction.
	 */
	public int getDirectionX() {
		// TO BE COMPLETED
	}

	/**
	 * Returns the y component of the direction of the object.
	 *
	 * @return y component of the direction.
	 */
	public int getDirectionY() {
		// TO BE COMPLETED
	}
}
