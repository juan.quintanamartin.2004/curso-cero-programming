package breakout;

import java.awt.event.KeyListener;

public interface GameView {

	/**
	 * Component's repaint method.
	 */
	void repaint();

	/**
	 * Sets the message to print upon the finalization of the game (depending on
	 * whether the player wins or loses).
	 * 
	 * @param msg Message to be printed.
	 */
	void setMessage(String msg);

	/**
	 * Updates the shown score.
	 * 
	 * @param score Value to be depicted.
	 */
	void updateScore(int score);

	/**
	 * Updates the shown stage.
	 * 
	 * @param stage Value to be depicted.
	 */
	void updateStage(int stage);
	
	/**
	 * Adds a key listener to control the key actions of the window.
	 * 
	 * @param ctrl Controller in charge of listening to the key actions
	 */
	void addKeyListener(KeyListener ctrl);
}
